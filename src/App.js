import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { Login, Main,Admin, Cadastro } from './views';
import './App.css';

class App extends React.Component {
  state = {
    loggedIn: false,
  }

  render() {
    const { loggedIn } = this.state;

    return (
      <div className="App">
        <Route exact path="/" render={() => (
          loggedIn
            ? <Redirect to="/Main" />
            : <Redirect to="/Login" />
        )} />
        <Route path='/Login' component={Login} />
        <Route path='/Main' component={Main} />
        <Route path='/Cadastro' component={Cadastro} />
        <Route path='/Admin' component={Admin} />
      </div>
    );
  }
}

export default App;
