import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import 'materialize-css/dist/css/materialize.min.css';
import 'materialize-css/dist/js/materialize.min';
import 'material-icons/css/material-icons.min.css';

ReactDOM.render((
  <BrowserRouter>

    <App />
  </BrowserRouter>
), document.getElementById('root'));

registerServiceWorker();
