import React from 'react';
import { Link } from 'react-router-dom';

import './Login.css';

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.handleEmail = this.handleEmail.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
  }

  state = {
    email: '',
    password: '',
  }

  handleEmail = email => this.setState({ email })

  handlePassword = password => this.setState({ password });

  render() {
    return (
      <div className="col s12 m6 l6">
        <div className="row center">
          <h1>Encostai</h1>
        </div>

        <div />

        <div className="row center">
          <div className="col s12 m12 l12">
            <div className="row">
              <div className="input-field col s12">
                <input id="email" type="email" className="validate" />
                <label for="email">Email</label>
              </div>

            </div>

            <div className="row">
              <div className="input-field col s12">
                <input id="password" type="password" className="validate" />
                <label for="password">Password</label>
              </div>
            </div>

            <div className="row center">
              <Link to="/Main">
                <a className="waves-effect waves-light btn enter-button">Entrar</a>
              </Link>
              <Link to="/Admin">
                <a className="waves-effect waves-light btn enter-button">Admin</a>
              </Link>
              <Link to="/Cadastro">
              <a className="waves-effect waves-light btn">Cadastrar</a>
              </Link>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default Login;
