import React from 'react';
import { Link } from 'react-router-dom';
import './Cadastro.css';

class Cadastro extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            confirmPassword: '',
            parkingName: '',
            isAccepted: true
        }

        this.handleEmail = this.handleEmail.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.handleConfirmPassword = this.handleConfirmPassword.bind(this);
        this.handleParkingName = this.handleParkingName.bind(this);
        this.handleCheckbox = this.handleCheckbox.bind(this);
    }

    handleEmail = email => this.setState({ email });
    handlePassword = password => this.setState({ password });
    handleParkingName = parkingName => this.setState({ parkingName });
    handleConfirmPassword = confirmPassword => this.setState({ confirmPassword });
    handleCheckbox = isAccepted => this.setState({ isAccepted });

    render() {
        return (
            <div className="col s12 m6 l6">
                <div className="row center">
                    <h1>Encostai</h1>
                </div>

                <div />

                <div className="row center">
                    <div className="col s12 m12 l12">
                        <div className="row">
                            <div className="input-field col s12">
                                <input id="parkingName" className="validate" />
                                <label for="parkingName">Nome do Estacionamento</label>
                            </div>

                        </div>

                        <div className="row">
                            <div className="input-field col s12">
                                <input id="email" type="email" className="validate" />
                                <label for="email">Email</label>
                            </div>
                        </div>

                        <div className="row">
                            <div className="input-field col s12">
                                <input id="password" type="password" className="validate" />
                                <label for="password">Senha</label>
                            </div>
                        </div>

                        <div className="row">
                            <div className="input-field col s12">
                                <input id="confirmPassword" type="password" className="validate" />
                                <label for="confirmPassword">Confirmar Senha</label>
                            </div>
                        </div>

                        <label>
                            Aceita os termos de serviço?:
                                <input name="isAccepted" type="checkbox" checked={this.state.isAccepted} onChange={this.handleCheckbox} />
                        </label>

                        <div className="row center">
                            <Link to="/Main">
                                <a className="waves-effect waves-light btn">Cadastrar</a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Cadastro;