import React from 'react';

class Details extends React.Component {
  render() {
    return (
      <div class="col s12">
        <div class="input-field col s12">
          <input placeholder="Número de pisos" id="floors" type="number" class="validate" />
          <label for="floors">Numero de Pisos</label>
        </div>
        <div class="input-field col s12">
          <input placeholder="CEP" id="park" type="number" class="validate" />
          <label for="park">Numero de vagas</label>
        </div>
        <div class="input-field col s12">
          <input placeholder="Complemento" id="complement" type="number" class="validate" />
          <label for="complement">Horário de funcionamento</label>
        </div>
      </div>
    );
  }
}

export default Details;
