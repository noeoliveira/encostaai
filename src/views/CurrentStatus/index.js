import React from 'react';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

class CurrentStatus extends React.Component {
  constructor() {
    super()
    this.state = {
      free: 0,
      occupied: 0,

    }
  }

  render() {
    return (
      <div>
        <Grid container spacing={40}>
          <Grid item xs={10}>
            <Paper>
              <Table >
                <TableBody>
                  <TableRow>
                    <TableCell><h6>Vagas disponiveis</h6></TableCell>
                    <TableCell numeric><h6>{this.state.free}</h6></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><h6>Vagas Ocupadas</h6></TableCell>
                    <TableCell numeric><h6>{this.state.occupied}</h6></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><h6>Vagas Total</h6></TableCell>
                    <TableCell numeric><h6>{this.state.free + this.state.occupied}</h6></TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </Paper>
          </Grid>
          <Grid container item xs={2} spacing={16} alignContent='center' >
            <Grid item xs={12} >
              <Button type="submit" variant="raised" color="primary">Entrou</Button>
            </Grid>
            <Grid item xs={12}>
              <Button type="submit" variant="raised" color="secondary" >Saiu</Button>
            </Grid>
          </Grid>
        </Grid>

      </div>
    );
  }
}

export default CurrentStatus;