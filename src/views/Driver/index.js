import React from 'react';
import Autosuggest from 'react-autosuggest';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Store from '@material-ui/icons/Store';

const data = [
    { label: 'Afghanistan' },
    { label: 'Aland Islands' },
    { label: 'Albania' },
    { label: 'Algeria' },
    { label: 'American Samoa' },
    { label: 'Andorra' },
    { label: 'Angola' },
    { label: 'Anguilla' },
    { label: 'Antarctica' },
    { label: 'Antigua and Barbuda' },
    { label: 'Argentina' },
    { label: 'Armenia' },
    { label: 'Aruba' },
    { label: 'Australia' },
    { label: 'Austria' },
    { label: 'Azerbaijan' },
    { label: 'Bahamas' },
    { label: 'Bahrain' },
    { label: 'Bangladesh' },
    { label: 'Barbados' },
    { label: 'Belarus' },
    { label: 'Belgium' },
    { label: 'Belize' },
    { label: 'Benin' },
    { label: 'Bermuda' },
    { label: 'Bhutan' },
    { label: 'Bolivia, Plurinational State of' },
    { label: 'Bonaire, Sint Eustatius and Saba' },
    { label: 'Bosnia and Herzegovina' },
    { label: 'Botswana' },
    { label: 'Bouvet Island' },
    { label: 'Brazil' },
    { label: 'British Indian Ocean Territory' },
    { label: 'Brunei Darussalam' },
]


// Ensina ao Autosuggest como calcular sugestões para qualquer valor de entrada fornecido.
const getSuggestions = value => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return inputLength === 0 ? [] : data.filter(text =>
        text.label.toLowerCase().slice(0, inputLength) === inputValue
    );
};

/* Quando a sugestão é clicada, a sugestão automática precisa preencher a entrada
  com base na sugestão clicada. Ensine Autosuggest como calcular o
  valor de entrada para cada sugestão dada.*/
const getSuggestionValue = suggestion => suggestion.label;

// Use sua imaginação para render sugestões.
const renderSuggestion = suggestion => (
    <Paper>
        <MenuItem>
            <ListItemIcon>
                <Store />
            </ListItemIcon>
            {suggestion.label}
        </MenuItem>
    </Paper>
);

class Parking extends React.Component {
    constructor() {
        super();

        /* Autosuggest é um componente controlado.
          Isso significa que você precisa fornecer um valor de entrada
          e um manipulador onChange que atualiza esse valor (veja abaixo).
          Sugestões também precisam ser fornecidas para o Autosuggest,
          e eles estão inicialmente vazios porque o Autosuggest está fechado.*/
        this.state = {
            value: '',
            suggestions: []
        };
    }

    onChange = (event, { newValue }) => {
        this.setState({
            value: newValue
        });
    };

    /* Autosuggest chamará essa função toda vez que você precisar atualizar sugestões.
     Você já implementou essa lógica acima, então apenas use-a.*/
    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: getSuggestions(value)
        });
    };

    // Autosuggest chamará essa função toda vez que precisar limpar as sugestões.
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    render() {
        const { value, suggestions } = this.state;

        // Autosuggest passará por todos esses adereços para a entrada.
        const inputProps = {
            placeholder: 'Pesquise os motoristas cadastrados',
            value,
            onChange: this.onChange
        };


        return (
            <Autosuggest
                suggestions={this.state.suggestions}
                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                getSuggestionValue={getSuggestionValue}
                renderSuggestion={renderSuggestion}
                inputProps={inputProps}
            />
        );
    }
}
export default Parking;