import React from 'react';
import { Switch, Route } from 'react-router-dom';

import './Main.css';
import { Nav } from '../../components';
import { GeneralData, Values, Details, CurrentStatus } from '../';

class Main extends React.Component {
  static tabs = [
    { label: 'Dados gerais', to: '/Main/GeneralData' },
    { label: 'Detalhes', to: '/Main/Details' },
    { label: 'Valores', to: '/Main/Values' },
    { label: 'Atualmente', to: '/Main/CurrentStatus' },
  ];

  render() {
    return (
      <div>
        <Nav tabs={Main.tabs} />

        <div className="general-container">
          <section>
            <Switch>
              <Route path='/Main/GeneralData' component={GeneralData} />
              <Route path='/Main/Details' component={Details} />
              <Route path='/Main/Values' component={Values} />
              <Route path='/Main/CurrentStatus' component={CurrentStatus} />
            </Switch>
          </section>
        </div>
      </div>
    );
  }
}

export default Main;