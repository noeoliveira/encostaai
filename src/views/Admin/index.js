import React from 'react';
import { Switch, Route } from 'react-router-dom';

import { NavAdmin } from '../../components';
import { Dashboard, Parking, Driver } from '../';

class Admin extends React.Component {
    static tabs = [
        { label: 'Dashboard', to: '/Admin/Dashboard' },
        { label: 'Estacionamento Cadastrado', to: '/Admin/Parking' },
        { label: 'Motorista Cadastrado', to: '/Admin/Driver' },
    ];

    render() {
        return (
            <div>
                <NavAdmin tabs={Admin.tabs} />
                <section>
                    <Switch>
                        <Route path='/Admin/Dashboard' component={Dashboard} />
                        <Route path='/Admin/Parking' component={Parking} />
                        <Route path='/Admin/Driver' component={Driver} />
                    </Switch>
                </section>

            </div>
        );
    }
}

export default Admin;