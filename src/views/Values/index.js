import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";


const state = {
  valueParking: '',
  parkingPrice: 0,
}
const CustomTableCell = withStyles(theme => ({
  head: {
    fontSize:14,
    backgroundColor:theme.palette.background.default

  },
  body: {
    fontSize: 14
  }
}))(TableCell);

const styles = theme => ({
  root: {
    width: "50%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto"
  },
  table: {
     minWidth: 100
  },
  row: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default
    }
  }
});

let id = 0;
function createData(name, values) {
  id += 1;
  return { id, name, values };
}
// valores que vão ser trazidos do banco de dados
const data = [
  createData("Até 1 hora", "R$"+6),
  createData("Até 3 horas", "R$"+10),
  createData("Dia", "R$" +20),
];

function CustomizedTable(props) {
  const { classes } = props;

  return (

    <Paper className={classes.root}>
      <Table className={classes.table}>
        <tr><td>
        <label for="valueParking">Defina aqui o horário do estacionamento</label>
                <input id="valueParking" type="text"  />
                
            </td><td>
              <label for="parkingPrice">Valor</label>
                <input id="parkingPrice" type="text"  />
            
                </td>
        </tr>
       </Table>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <CustomTableCell>Horário</CustomTableCell>
            <CustomTableCell numeric>Valores</CustomTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map(n => {
            return (
              <TableRow className={classes.row} key={n.id}>
                <CustomTableCell component="th" scope="row">
                  {n.name}
                </CustomTableCell>
                <CustomTableCell numeric>{n.values}</CustomTableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );

}

CustomizedTable.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CustomizedTable);
