import Login from './Login';
import Main from './Main';
import Cadastro from './Cadastro';
import GeneralData from './GeneralData';
import Values from './Values';
import CurrentStatus from './CurrentStatus';
import Details from './Details';
import Admin from './Admin';
import Dashboard from './Dashboard';
import Parking from './Parking';
import Driver from './Driver';

export {
  Login,
  Main,
  Cadastro,
  GeneralData,
  Values,
  CurrentStatus,
  Details,
  Admin,
  Dashboard,
  Parking,
  Driver,
};
