import React from 'react';

class Splash extends React.Component {
  render() {
    return (
      <div class="col s12">
        <div class="input-field col s12">
          <input placeholder="Nome" id="name" type="text" class="validate" />
          <label for="name">Nome</label>
        </div>
        <div class="input-field col s12">
          <input placeholder="CEP" id="cep" type="text" class="validate" />
          <label for="cep">CEP</label>
        </div>
        <div className="row">
          <div class="input-field col s6">
            <input placeholder="Endereço" id="address" type="text" class="validate" />
            <label for="address">Endereço</label>
          </div>
          <div class="input-field col s6">
            <input placeholder="Numero" id="number" type="number" class="validate" />
            <label for="number">Numero</label>
          </div>
        </div>
        <div class="input-field col s12">
          <input placeholder="Complemento" id="complement" type="text" class="validate" />
          <label for="complement">Complemento</label>
        </div>
      </div>
    );
  }
}

export default Splash;
