import React from 'react';
import { Link } from 'react-router-dom';
import { AppBar, Drawer, IconButton, Toolbar, Typography, ListItem, List } from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import MenuIcon from '@material-ui/icons/Menu';

class NavAdmin extends React.Component {
    constructor() {
        super();
        this.state = {
            open: false
        }
    }

    renderTabItems = items => items.map(({ label, to, i }) => (

        <Link to={to} key={to.toString()} >
            <ListItem button onClick={this.handleDrawer}>
                {label}
            </ListItem>
        </Link>

    ));

    handleDrawer = () => {
        this.setState({
            open: !this.state.open
        });
    };
    render() {
        const { tabs } = this.props;
        return (
            <div>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton aria-label="Menu" color="inherit"
                            onClick={this.handleDrawer}>
                            <MenuIcon />
                        </IconButton>

                        <Typography variant="title" color="inherit">Encostai</Typography>
                    </Toolbar>
                </AppBar>

                <Drawer open={this.state.open} onClose={this.handleDrawer}>
                    <IconButton id="tooltip-right-start" onClick={this.handleDrawer}>
                        <ChevronLeftIcon />
                    </IconButton>
                    <List>
                        {this.renderTabItems(tabs)}
                    </List>
                </Drawer>



            </div>
        );
    }
}
export default NavAdmin;