import React from 'react';
import { Link } from 'react-router-dom';

class Nav extends React.Component {
  renderTabItems = items => items.map(({ label, to }) => (
    <li class="tab">
      <a>
        <Link to={to}>{label}</Link>
      </a>
    </li>
  ));

  render() {
    const { tabs } = this.props;

    return (
      <div>
        <nav class="nav-extended">
          <div class="nav-wrapper">
            <a href="#" class="brand-logo">Encostai</a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
          </div>
          <div class="nav-content">
            <ul class="tabs tabs-transparent">
              {this.renderTabItems(tabs)}
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default Nav;
